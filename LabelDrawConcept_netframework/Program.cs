﻿using r2pLibs.cgi;
using r2pLibs.cgi.Interfaces;
using r2pLibs.cgi.Sprites;
using r2pLibs.cgi.Types;
using r2pLibs.Common.Logging;
using r2pLibs.Consoles;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace LabelDrawConcept
{
	class Program : ConsoleHelperBase
	{
		static double timeStart = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
		static string ImagePath = Path.Combine("assets", "Marker_Sprites.png");
		static void Main()
		{
			var spriteSheet = new Spritesheet(5100, 3400, GetSprites());


			Log("Main :: start");
			spriteSheet.Draw();

			Log("Main :: End");
			HandleExit();
		}

		static List<ISprite> GetSprites()
		{
			Log("GetSprites :: start");
			var sprites = new List<ISprite>();
			var image = new Bitmap(ImagePath, false);
			for (var i = 0; i < 1000; i++)
			{
				var font = new Font("Arial", 25, FontStyle.Regular, GraphicsUnit.Pixel);
				var text = $"WWWWWWWWWWWWWWW";	
				sprites.Add(new Sprite(new SpriteOptions
				{
					Text = text,
					SouceImage = image,
					Font = font,
					Hight = 30,
					Width = 500,
					SourceOffset = new Offset { X = 0, Y = 0 }
				}));
			}

			Log("GetSprites :: end");
			return sprites;
		}

		static void Log(string text)
		{
			var time = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds - timeStart;
			CLog.Info($"{(int)time,-10} | {text}");
		}
	}
}
