﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using static LabelDrawConcept.Spritesheet;

namespace LabelDrawConcept
{
	public class Sprite
	{
		public int Width { get; }
		public int Height { get; }


		string Text { get; }
		int Rotation { get; }
		SpriteColours Colour { get; }

		Font TextFont { get; } = new Font("Myriad Pro", 75, FontStyle.Regular, GraphicsUnit.Pixel);

		const int CenterWidth = 10;
		const int FrontWidth = 200;
		const int EndWidth = 45;

		const int FrontHeight = 178;
		const int CenterHeight = 141;
		const int EndHeight = 141;

		int CenterCount { get; set; }

		public Sprite(string text, int rotation, SpriteColours colour)
		{
			Text = text;
			Rotation = rotation;
			Colour = colour;
			Width = FindWidth(Text);
			Height = 178;
		}

		public void Draw(Image source, Graphics sheet, Offset offset)
		{
			DrawImage(sheet, source,  offset);
			sheet.DrawString(Text, TextFont, Brushes.Black, new PointF { X = offset.X + FrontWidth + 5, Y = offset.Y + 5 });

		}
		void DrawImage(Graphics sprite, Image source, Offset offset)
		{
			var centerAreaWidth = CenterWidth * CenterCount;
			var endCalculatedX = FrontWidth + (CenterWidth * CenterCount);

			var frontDist = new Rectangle(offset.X + 0, offset.Y + 0, FrontWidth, FrontHeight);
			var centerDist = new Rectangle(offset.X + FrontWidth, offset.Y + 0, centerAreaWidth, CenterHeight);
			var endDist = new Rectangle(offset.X + endCalculatedX, offset.Y + 0, EndWidth, EndHeight);

			var imageAttr = new ImageAttributes();
			imageAttr.SetWrapMode(WrapMode.Clamp);

			sprite.DrawImage(source, frontDist, 0, 0, FrontWidth, 178, GraphicsUnit.Pixel, imageAttr);
			sprite.DrawImage(source, centerDist, 0, 330, CenterWidth - 1, 141, GraphicsUnit.Pixel, imageAttr);
			sprite.DrawImage(source, endDist, 0, 180, EndWidth, 141, GraphicsUnit.Pixel, imageAttr);

		}

		int FindWidth(string text)
		{
			using (Image img = new Bitmap(1, 1))
			{
				using var drawing = Graphics.FromImage(img);
				var textSize = drawing.MeasureString(text, TextFont);
				CenterCount = (int)Math.Ceiling(textSize.Width / CenterWidth);
			}
			return (CenterWidth * CenterCount) + FrontWidth + EndWidth;
		}
	}
}
