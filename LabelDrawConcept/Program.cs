﻿using r2pCore.Common.Logging.Basic;
using r2pCore.Consoles;
using System;
using System.Collections.Generic;
using System.IO;

namespace LabelDrawConcept
{
	class Program : ConsoleHelperBase
	{
		static void Main()
		{
			var imagePath = Path.Combine("assets", "Marker_Sprites.png");

			var spriteSheet = new Spritesheet(18000, 9000, GetSprites());
			spriteSheet.SetSource(imagePath);
			spriteSheet.Draw();

			Log.Console.Information("End", DateTime.Now.ToString("ss:fff"));
			HandleExit();
		}

		static List<Sprite> GetSprites()
		{
			var sprites = new List<Sprite>();
			for (var i = 0; i < 1000; i++)
				sprites.Add(new Sprite($"test image hjgjhgh {i}", 0, SpriteColours.Green));

			return sprites;
		}
	}
}
