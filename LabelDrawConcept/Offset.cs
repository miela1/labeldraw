﻿namespace LabelDrawConcept
{
	public partial class Spritesheet
	{
		public class Offset
		{
			public int X { get; set; }
			public int Y { get; set; }
		}
	}
}
