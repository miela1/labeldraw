﻿namespace LabelDrawConcept
{
	public partial class Spritesheet
	{
		public class Size
		{
			public int Width { get; set; }
			public int Height { get; set; }
		}
	}
}
