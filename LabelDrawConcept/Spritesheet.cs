﻿using r2pCore.Common.Logging.Basic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace LabelDrawConcept
{
	public partial class Spritesheet
	{
		Image ImageSource { get; set; }

		public List<Sprite> Sprites { get; }
		public Size SheetSize { get; }

		public Spritesheet(int width, int height, List<Sprite> sprites)
		{

			Sprites = new List<Sprite>();
			Sprites.AddRange(sprites);

			SheetSize = new Size
			{
				Width = width,
				Height = height
			};
		}

		public void SetSource(string path)
		{
			ImageSource = new Bitmap(path, false);
		}

		public void Draw()
		{
			Log.Console.Information("Start Draw", DateTime.Now.ToString("ss:fff"));

			Image sheet = new Bitmap(SheetSize.Width, SheetSize.Height);
			using (Graphics graphics = Graphics.FromImage(sheet))
			{
				var offset = new Offset();
				graphics.Clear(Color.Transparent);

				foreach (var sprite in Sprites)
				{
					sprite.Draw(ImageSource, graphics, offset);
					offset.X += sprite.Width + 5;
					if (offset.X + sprite.Width + 5 <= SheetSize.Width)
						continue;

					offset.X = 0;
					offset.Y += sprite.Height;
				}
			}
			Log.Console.Information("Start save", DateTime.Now.ToString("ss:fff"));
			sheet.Save("test.png", ImageFormat.Png);
			sheet.Dispose();
		}
	}
}
