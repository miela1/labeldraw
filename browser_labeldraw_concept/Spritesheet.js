class Spritesheet {
    constructor(width, height, sprites) {
        this.Sprites = [];
        this.Sprites = sprites;

        this.SheetSize = {
            width,
            height
        };
    }

    async SetSource(path) {
        this.ImageSource = await this.LoadImage(path);
    }

    Draw() {
        var canvas = this.CreateCanvas(this.SheetSize.width, this.SheetSize.height);
        var offset = { x: 0, y: 0 };
        for (var sprite of this.Sprites) {
            sprite.Draw(this.ImageSource, canvas, offset);
            offset.x += sprite.Width + 5;
            if (offset.x + sprite.Width + 5 <= this.SheetSize.width)
                continue;

            offset.x = 0;
            offset.y += sprite.Height;
        }
        console.log("done");
        var t = canvas.toDataURL("image/png");
        console.log("data url done");
        return t;

    }
    CreateCanvas(width, height) {
        var c = document.createElement("canvas");
        c.width = width;
        c.height = height;

        return c;
    }
    async LoadImage(url) {
        return new Promise((resolve, reject) => {
            try {
                var image = new Image();
                image.addEventListener('load', function () {
                    resolve(image);
                }, false);
                image.src = url; // Set source path
            } catch (ec) {
                reject(ec);
            }
        });
    }
}