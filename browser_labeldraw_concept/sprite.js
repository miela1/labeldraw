class Sprite {
    constructor(text, rotation) {
        //Not the ecma 6 way, nut firefox does not suppot fields and properties directly in class
        // the proper way wuld have been eg. get TextFont() {return "75px Tahoma"; } or shorthand TextFont = "75px Tahoma" outside the constructor
        this.TextFont = "75px Tahoma";
        this.CenterWidth = 10;
        this.FrontWidth = 200;
        this.EndWidth = 45;
        this.FrontHeight = 178;
        this.CenterHeight = 141;
        this.EndHeight = 141;

        
        this.Text = text;
        this.Rotation = rotation;
        this.Width = this.FindWidth(text);
        this.Height = 178;
    }
    Draw(source, canvas, offset) {
        var sprite = canvas.getContext('2d');
        this.DrawImage(sprite, source, offset);
        sprite.font = this.TextFont;
        //sprite.fillText(this.Text, this.FrontWidth + offset.x, 75 + offset.y);
        return canvas;
    }
    DrawImage(sprite, source, offset) {
        var centerAreaWidth = this.CenterWidth * this.CenterCount;
        var endCalculatedX = this.FrontWidth + (this.CenterWidth * this.CenterCount);

        sprite.drawImage(source, 0, 0, this.FrontWidth, this.FrontHeight, offset.x + 0, offset.y + 0, this.FrontWidth, this.FrontHeight);
        sprite.drawImage(source, 330, 0, this.CenterWidth, this.CenterHeight, offset.x + this.FrontWidth, offset.y + 0, centerAreaWidth, this.CenterHeight);
        sprite.drawImage(source, 180, 0, this.EndWidth, this.EndHeight, offset.x + endCalculatedX, offset.y + 0, this.EndWidth, this.EndHeight);
    }

    FindWidth(text) {
        var tmpCanvas = document.createElement("canvas");
        var tmpCtx = tmpCanvas.getContext("2d");
        tmpCtx.font = this.TextFont;
        var textSize = tmpCtx.measureText(text);

        this.CenterCount = Math.ceil(textSize.width / this.CenterWidth);
        return (this.CenterWidth * this.CenterCount) + this.FrontWidth + this.EndWidth;
    }
}