class Program {
    static Main() {
        var logLevel = 3;
        var log = new Logging(logLevel);
        log.OverwriteDefaults();
        this.Run();
    }

    static Run() {
        var sprites = [];
        for (var i = 0; i <= 1000; i++) {
            sprites.push(new Sprite(`test image ${i}`, 0));
        }

        var sp = new Spritesheet(18000, 9000, sprites);
        sp.SetSource("assets/Marker_Sprites.png").then(() => {
            console.log("run");
            var resultData = sp.Draw();

            var resultEl = document.getElementById("result");
            var resultImage = document.createElement("img");
            resultImage.src = resultData;

            resultEl.appendChild(resultImage);
        });
    }
}