class Logging {
    constructor(level) {
        this.LogLevel = level
    }

    OverwriteDefaults() {
        this.LogOrg = console.log;
        console.log = this.Log.bind(this);
        console.error = this.Error.bind(this);
        console.info = this.Info.bind(this);
    }
    Log(text, args) { this.WriteLog(text, args, 0); }
    Info(text, args) { this.WriteLog(text, args, 1); }
    Error(text, args) { this.WriteLog(text, args, 2); }

    WriteLog(text, args, level) {
        if (level > this.LogLevel)
            return;
        if (args)
            this.LogOrg(`${Date.now()} : ${text}`, JSON.stringify(args));
        if (!args)
            this.LogOrg(`${Date.now()} : ${text}`);
    }
}