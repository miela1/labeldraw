var { createCanvas } = require('canvas');

class Sprite {
    TextFont = "75px Tahoma";
    CenterWidth = 10;
    FrontWidth = 200;
    EndWidth = 45;
    FrontHeight = 178;
    CenterHeight = 141;
    EndHeight = 141;

    constructor(text, rotation) {
        this.Text = text;
        this.Rotation = rotation;
        this.Width = this.FindWidth(text);
        this.Height = 178;
    }
    Draw(source, canvas, offset) {
        var sprite = canvas.getContext('2d');
        this.DrawImage(sprite, source, offset);
        sprite.font = this.TextFont;
        sprite.fillText(this.Text, this.FrontWidth, 0);
        return canvas;
    }
    DrawImage(sprite, source, offset) {
        var centerAreaWidth = this.CenterWidth * this.CenterCount;
        var endCalculatedX = this.FrontWidth + (this.CenterWidth * this.CenterCount);

        sprite.drawImage(source, 0, 0, this.FrontWidth, this.FrontHeight, offset.x + 0, offset.y + 0, this.FrontWidth, this.FrontHeight);
        sprite.drawImage(source, 330, 0, this.CenterWidth, this.CenterHeight, offset.x + this.FrontWidth, offset.y + 0, centerAreaWidth, this.CenterHeight);
        sprite.drawImage(source, 180, 0, this.EndWidth, this.EndHeight, offset.x + endCalculatedX, offset.y + 0, this.EndWidth, this.EndHeight);
    }

    FindWidth(text) {
        var tmpCanvas = createCanvas(1, 1);
        var tmpCtx = tmpCanvas.getContext("2d");
        var textSize = tmpCtx.measureText(text);

        this.CenterCount = Math.ceil(textSize.width / this.CenterWidth);
        return (this.CenterWidth * this.CenterCount) + this.FrontWidth + this.EndWidth;
    }
}
module.exports = Sprite;