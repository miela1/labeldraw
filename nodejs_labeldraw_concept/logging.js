var chalk = require("chalk");
class Logging {
    constructor(level) {
        this.LogLevel = level
    }

    OverwriteDefaults() {
        this.oldLog = console.log;
        console.log = this.Log;
        console.error = this.Error;
        console.info = this.Info;
    }
    Log = (text, args) => this.WriteLog(text, args, chalk.green, 0)
    Info = (text, args) => this.WriteLog(text, args, chalk.yellow, 1)
    Error = (text, args) => this.WriteLog(text, args, chalk.red, 2)

    WriteLog(text, args, colour, level) {
        if (level > this.LogLevel)
            return;
        if (args)
            this.oldLog(colour(`${Date.now()} : ${text}`), JSON.stringify(args));
        if (!args)
            this.oldLog(colour(`${Date.now()} : ${text}`));
    }
}
module.exports = Logging;