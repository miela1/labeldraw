var Logging = require('./logging.js');
var Sprite = require("./sprite.js");
var Spritesheet = require("./Spritesheet.js");

class Program {
    static Main(args) {
        var logLevel = 0;
        var logIndex = args.indexOf("loglevel")
        if(logIndex > -1) {
            logLevel = parseInt(args[logIndex +1], 10);
        }
        var log = new Logging(logLevel);
        log.OverwriteDefaults();
        this.Run();
    }

    static Run() {
        var sprites = [];
        for (var i = 0; i <= 1000; i++) {
            sprites.push(new Sprite(`test image ${i}`, 0));
        }

        var sp = new Spritesheet(18000, 9000, sprites);
        sp.SetSource("assets/Marker_Sprites.png").then(()=>{
            console.log("run");
            sp.Draw();
        });
    }
}
Program.Main(process.argv.slice(2));