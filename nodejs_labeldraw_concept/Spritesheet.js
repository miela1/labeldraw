var { createCanvas, loadImage } = require('canvas');

var fs = require("fs");
class Spritesheet {
    constructor(width, height, sprites) {
        this.Sprites = [];
        this.Sprites=sprites;

        this.SheetSize = {
            width,
            height
        };
    }

    async SetSource(path) {
        this.ImageSource = await loadImage(path);
    }

    Draw() {
        var canvas = createCanvas(this.SheetSize.width, this.SheetSize.height);
        var offset = { x: 0, y: 0 };
        for (var sprite of this.Sprites) {
            sprite.Draw(this.ImageSource, canvas, offset);
            offset.x += sprite.Width + 5;
            if (offset.x + sprite.Width + 5 <= this.SheetSize.width)
                continue;

            offset.x = 0;
            offset.y += sprite.Height;
        }
        console.log("done");
        var imageBuffer = canvas.toBuffer('image/png', { compressionLevel: 3, filters: canvas.PNG_FILTER_NONE });
        console.log("done");
        var t = canvas.toDataURL();
        console.log("done");        
        fs.writeFile("test.png", imageBuffer, ()=>{
            console.log("yay2");
        });
    }
}

module.exports = Spritesheet;